<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>fitness institute</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="">
	<style type="text/css">
		/* Orange */
.warning {
  border-color: black;
  color: orange;
}

.warning:hover {
  background: #FFD700;
  color: black;
}
.btnn {

  border: 0px solid ;
  border-radius: 16px;
  background-color: black;
  color: white;
  padding: 14px 28px;
  font-size: 16px;
  cursor: pointer;
}

	</style>
</head>
<body style="font-family:poppins">
		<!-- navbar started -->
<?php 
	include('nav_header.php');
?>
<!-- navbar ended -->

<div class="container-fluid mt-4">
<!-- 	
	<h1 style="font-family: impact" class="text-center text-dark bg-warning">GUTS & CUTS FITNESS HUB</h1>
	<div class="row">
		<div class="col-lg-6 col-md-12 text-white bg-dark">
			<b>A person takes up a gym membership to get fit. He may have specific goals either to lose weight, gain weight, general maintenance or maybe sport specific or even medical.
			<br><br>
			We all have different bodies and each individual is different so sometimes it is beneficial to hire a personal trainer.
			<br><br>
			The skilled guidance of the trainer keeps you involved in a variety of different fitness exercises so that you don’t get astray. Fitness trainers are thorough professionals who know how to keep people motivated to follow the fitness programs. They plan the fitness program according to your individual needs. They will constantly keep you inspired and stimulated by keeping you involved in interesting activities.
			<br><br>
			A personal trainer can help you reach your fitness goals. He is a coach, a counselor, a teacher, a source of inspiration and motivation.</b>
		</div>
		<div class="col-lg-6 col-md-12 text-white bg-dark">
			<img src="images\pt1.jpg" class="w-100 img-fluid" alt="">
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-12 text-white bg-dark">
			<img src="images\ptg.jpg" class="w-100 img-fluid" alt="">
		</div>
		<div class="col-lg-6 col-md-12   text-white bg-dark">
			<b>Why is a Personal Trainer needed
			<br><br>
			A trainer will:
			<br><br>
			<ul type="bullets">
			<li>Assess your individual abilities and needs.</li>
			<li>Personalize your workouts so that they help you achieve your goals.</li>
			<li>Ensure you are doing exercises correctly and provide instant feedback on how to adjust your posture, motion, and exertion to get maximum results and prevent injury.</li>
			<li>Keep changing your workouts as you gain in strength and endurance or if a certain exercise is not working for you.
			Keep your workouts interesting and fun, introducing you to using different equipment to work the same muscle groups.</li>
			</ul>
			<br><br>
			Becoming a trainer is actually very easy. Guts&Cuts Gym Fitness Institute conducts courses in personal training. This is a three month program with theory & Practical followed by an internship. You just need to be 12th passed as a basic qualification.</b>
		</div>
	</div>
	<!-- ended
	<hr style="border-width: 2px;border-color: black">
	<!-- PERSONAL TRAINING -->
		<div class="row  text-white bg-dark">
		<div class="col-lg-6 col-md-12">
			<h2 class=" text-center" style="font-family: impact">PERSONAL TRAINING</h2>
			<b>
				The Gold’s Gym Fitness Institute GGFI is India’s first International Fitness Management Institute. It offers certificate courses in Fitness Management, Personal Training and Spinning Instructing.
				<br><br>
				It covers a range of subjects to comprehensively address and ensure the best international brand practices in India.
				<br><br>
				The 3 month courses are monitored by qualified personnel of Guts&Cuts Gym, and also hosts guest lectures by the biggest names in the international fitness industry with guaranteed internships and a firsthand opportunity to experience fitness training from the world leaders in it.
				<br><br>
				The G&CFI offers the most exciting and up-to-date education pathway to gain industry leading qualifications as well as ongoing training in all aspects of the fitness industry.
				</b>
				<BR><BR><BR>

		</div>
		<div class="col-lg-6 col-md-12">
			<h2 class=" text-center" style="font-family: impact">BENEFITS</h2>
			<b><ul type="bullets">
				<li>You can earn anything between Rs. 8000 – Rs. 50000 depending on what you are doing.</li>
				<li>The job is flexible. You can work in the gym or freelance outside the gym.</li>
				<li>The pros of working within a gym is you have immediate access to hundreds of potential clients, access to top of the range equipment and, gym dependant, you will have the support of advertising.</li>
				<li>If you are working outside the gym, you are not restricted to one location, it can be extremely convenient for your client to train in the comfort of their own home and the hours you work are entirely yours to decide.</li>
				<li>And mainly you get Job satisfaction.</li>
				<li>You can have a positive impact on people’s lives. You can help them increase strength, improve appearance or become a healthier person, whatever it may be you are there to help.</li>
				
				</ul></b>
				<BR><BR><BR>
				<form>
				<button style="font-family: impact" formaction="join_now.php" type="submit" class="btnn warning mb-2">JOIN TODAY</button>
			</form>
		</div>
	</div>
 --> -->	<!-- eneded PERSONAL TRAINING -->
</div>
<!-- FOOTER -->
<?php 
	include('footer.php');
?>
<!-- FOOTER ENDED -->

  <!-- Bootstrap css plugin -->

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script><style type="text/css" media="screen">

</body>
</html>