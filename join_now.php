<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Join Now</title>
	<link rel="stylesheet" href="css/style.css">
	<style>
	.btnall
			{
				width: 300px;
				background: linear-gradient(90deg,gold,orange);
				font-family: impact;
				font-size: 25px;
				box-shadow: 2px 2px 2px;
			}
		.btnall:hover
		{
			box-shadow: 2px 2px 4px;
			letter-spacing: 1px;
			font-size: 20px;
			background: linear-gradient(100deg,orange,gold);
			transition: 1s;
		}	
	</style>
		<script  type = "text/javascript">

      // Form validation code will come here.
      function validate() {
      	if( document.myForm.department.value == "-1" ) {
            alert( "Please provide Department!" );
            return false;
         }
         if( document.myForm.branch.value == "-1" ) {
            alert( "Please provide Branch!" );
            return false;
         }
      
         if( document.myForm.name.value == "" ) {
            alert( "Please provide your name!" );
            document.myForm.name.focus() ;
            return false;
         }
         if( document.myForm.email.value == "" ) {
            alert( "Please provide your Email!" );
            document.myForm.email.focus() ;
            return false;
         }
         if( document.myForm.phone.value == "" || isNaN( document.myForm.phone.value ) ||
            document.myForm.phone.value.length != 10 ) {
            
            alert( "Please provide your phone number and only enter 10 numbers" );
            document.myForm.phone.focus() ;
            return false;
         }
         
         var emailID = document.myForm.email.value;
         atpos = emailID.indexOf("@");
         dotpos = emailID.lastIndexOf(".");
         
         if (atpos < 1 || ( dotpos - atpos < 2 )) {
            alert("Please enter correct email ID")
            document.myForm.email.focus() ;
            return false;
         }
         return( true );
      }



</script>

</head>
<body  style="background-image: linear-gradient(to bottom, gold,orange);">
	<!-- navbar started -->
<?php 
	include('nav_header.php');
?>
<?php 
	include('db/db_setup.php');
	$data = get_all_data_from_table('branch');

	
		# code...
	
?>
<!-- navbar ended -->

<!-- container starts-->

<div class="container mt-4" >
	<hr>
	<h1 style="font-family: impact;margin-top:100px" class="text-center">BECOME A PERSONAL TRAINER TODAY</h1>
	<hr>
	<div class="row mb-2 bg-warning" >
		<div class="col-lg-8 col-md-12">
			<h1>GUTS & CUTS FITNESS HUB</h1>
			<B>G&C GYM FITNESS INSTITUTE OFFERS CERTIFICATION IN ADVANCE PERSONAL TRAINING FOR PEOPLE INTERESTED IN MAKING A CAREER IN THE HEALTH AND FITNESS INDUSTRY!</B><BR><BR>
			<b>YOU WILL:</b><BR><BR>
			<ul type="bullets">
				<li>GET TRAINED TO TRAIN</li>
				<li>BE A ROLE MODEL</li>
				<li>HAVE A SUSTAINABLE CAREER</li>
				<li>GET A FASTER RETURN ON INVESTMENT</li>
				<li>HAVE A HIGHER EARNING POTENTIAL</li>
				<li>HAVE A FASTER GROWTH OPPORTUNITIES</li>
				<li>GET A RECOGNIZED CERTIFICATION</li>
				<li>ENHANCED CREDIBILITY</li>
			</ul>
			<br><br>
			<b>AND ABOVE ALL CREATE A POSITVE IMPACT ON PEOPLES LIVES</b>
		</div>
		<div class="col-lg-4 col-md-12">
			<form  action="confirmation.php" onsubmit = "return(validate());" name="myForm" method="post" accept-charset="utf-8">
			
				<select class="form-control" name="department" class="bg-dark ">
				<option value="-1" selected>[SELECT A DEPARTMENT]</option>
					<option value="General Manager Projects">General Manager Projects</option>
					<option value="Sales Manager">Sales Manager</option>
					<option value="Membership Consultants (Sales)">Membership Consultants (Sales)</option>
					<option value="Personal Trainers">Personal Trainers</option>
					<option value="Fitness Instructors">Fitness Instructors</option>
					<option value="Group Exercise Coordinators">Group Exercise Coordinators </option>
					<option value="Group Exercise Instructors">Group Exercise Instructors</option>
					<option value="Front of House - Receptionists">Front of House - Receptionists</option>
				</select>
			
				<br>

				<select class="form-control" name="branch" class="bg-dark ">
				<option value="-1" selected>[SELECT A DEPARTMENT]</option>
					<?php 
					for ($i=0; $i <count($data) ; $i++)
					 { 
					 	?> 
					<option value="<?php echo $data[$i]['branch_name']; ?> | <?php echo $data[$i]['state']; ?>">
						<?php echo $data[$i]['branch_name']; ?> | <?php echo $data[$i]['state']; ?>
							
						</option>
					<?php
					}
					?>
					<!-- <option value="2"></option>
					<option value="3">Membership Consultants (Sales)</option>
					<option value="4">Personal Trainers</option> -->
				</select>
				<br>
				<input  class="form-control" type="text" id="name" name="name" placeholder="NAME" size="60px">
				<br>
				<input class="form-control" type="email" id="email" name="email" placeholder="EMAIL" size="60px">
				<br>
				<input class="form-control" type="number" id="phone" name="phone" placeholder="CONTACT NUMBER" maxlength="10" size="60px">
				<br>
				<textarea class="form-control" name="add" id="add" cols="30" rows="10" placeholder="ANY MESSAGE(OPTIONAL)"></textarea>
				<br>
				<!-- <input class="form-control warning" type="submit"> -->
				<center><button type="submit"  class="btn btnall" >Submit </button></center>
		</form>
		</div>
	</div>
</div>
<!-- container ended -->
<!-- footer -->
<?php 

	include('footer.php');
?>

<!-- footer ended -->



<!-- Bootstrap css plugin -->

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>

</body>
</html>