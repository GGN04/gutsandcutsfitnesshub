<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Classes</title>
	<link rel="stylesheet" href="">
	<style>
		.table-content{
			font-family: poppins;
			border-collapse: collapse;
			margin: 25px 0;
			font-size: 0.9em;
			min-width: 400px;
			border-radius: 5px 5px 0 0;
			overflow: hidden;
			box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
		}
		.table-content th{
			background-color: gold;
			color :  white;
			text-align: left;
			font-weight: bold;
		}

		.table-content th,.table-content td{
			padding: 12px 15px;
		}
		
		.table-content tbody tr{
			border-bottom: 1px solid orange;
		}
		.table-content tbody tr:nth-of-type(even){
			background-color: orange;
		}

		.table-content tbody tr:last-of-type{
			border-bottom: 2px solid gold;
		}
		.table-content tbody tr:active-row{
			font-weight: bold;
			color:black;
		}
		.table-content td:hover{
			cursor: pointer;
			color: white;
			padding: 12px 15px;
		}

	</style>
</head>
<body style="color: white;font-family: poppins">
	<?php
		include('db/db_setup.php');
		include('scripts.php');
		include('nav_header.php');
	?>
	<div class="container-fluid">
	<?php
					global $conn;
					$data = array();
					$query = "SELECT * FROM `classes` order by id asc";
					if ($result = mysqli_query($conn,$query)) {
						while($row = mysqli_fetch_assoc($result)){
							$data[] = $row;
						}
					}else{
						echo "unable to fetch data from database";
					}
	?>
	<br><br><br><br><br>
	<center>
	<table border="0" class="table-content text-dark" style="font-size:20px;background-image: linear-gradient(90deg,gold,orange)"> 
		<thead>
			<tr>
				<th colspan=8 class="text-center text-warning bg-dark"><h3>Classes</h3></th>
			</tr>

			<tr>
				<th class="text-info">Time</th>
				<th class="text-danger">Monday</th>
				<th class="text-success">Tuesday</th>
				<th class="text-primary">Wednesday</th>
				<th class="text-info">Thursday</th>
				<th class="text-danger">Friday</th>
				<th class="text-success">Saturday</th>
				<th class="text-primary">Sunday</th>
			</tr>
		</thead>
		<tbody>
			<?php
			for ($i=0; $i <count($data) ; $i++) { 
				$num = $i+1;
				echo "<tr>";
				echo "<td>".$data[$i]['time']."</td>";
				echo "<td>".$data[$i]['monday']."</td>";
				echo "<td>".$data[$i]['tuesday']."</td>";
				echo "<td>".$data[$i]['wednesday']."</td>";
				echo "<td>".$data[$i]['thursday']."</td>";
				echo "<td>".$data[$i]['friday']."</td>";
				echo "<td>".$data[$i]['saturday']."</td>";
				echo "<td>".$data[$i]['sunday']."</td>";

				echo "</td>";
				echo "</tr>";
			}
			?>
		</tbody>
	</table>
</center>
</div>
	<?php
		include('footer.php');
	?>
</body>
</html>