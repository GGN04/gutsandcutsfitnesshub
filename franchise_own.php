<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Franchise Own</title>
	<link rel="stylesheet" href="">
	<?php
		include('scripts.php');
	?>
<style>

h1{
	font-family: impact;	
}

.bg{
	background-image:linear-gradient(to right,white,grey);
	opacity: 1;
}

.btnall{
    border: 0px solid;
    background: linear-gradient(90deg,gold,orange);
    font-family: impact;
    box-shadow: 2px 2px 2px;
    text-decoration: none;
    color: black
}

.btnall:hover{
      box-shadow: 2px 2px 4px;
      letter-spacing: 0px;
      font-size: 17px;
      background: linear-gradient(100deg,orange,gold);
      transition: 0.1s;
      text-decoration: none;
      color: black
    }
    .divover:hover{
    	color: white;
    	background-color: black;
    	cursor: pointer;
    }
    .not-on-mobile{
    	display: block;
    }
    @media (max-width: 925px) {
    	.not-on-mobile{
    		display: none;
    	}
    }
    @media (max-width: 370px){
    	.not-at-small-screen{
    		display: none;
    	}
    }
</style>
</head>
<body  style="font-family:poppins">
	<?php
		include('nav_header.php');
	?>
	<div class="text-center text-warning m-4">
		<br>
		<h1>ABOUT GUTS & CUTS</h1>
		<p>Premium and High end fitness clubs with state of the art machines & facilities.</p>
	</div>
	<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 text-center  bg-warning divover" style="border: 5px solid;border-radius:120px;border-bottom-right-radius: 50px;border-bottom-left-radius: 50px ">
			<h2>2016<br>Founded</h2>
			<caption><b>
				IN INDIA
			</b></caption>
		</div>
		<div class="col-md-4 text-center  bg-warning divover" style="border: 5px solid;border-radius:120px;border-bottom-right-radius: 50px;border-bottom-left-radius: 50px ">
			<h2>4<br>CLUBS</h2>
			<caption><b>
				ACROSS INDIA</b></caption>
		</div>
		<div class="col-md-4 text-center  bg-warning divover" style="border: 5px solid;border-radius:120px;border-bottom-right-radius: 50px;border-bottom-left-radius: 50px ">
			<h2>10<br>LACS</h2>
			<caption><b>
				MEMBERS
			</b></caption>
		</div>
	</div>

<!-- OFFERINGS & FACILITIES -->

<div class="bg-white text-black bg not-at-small-screen">
	<h1 class="text-center pt-4 mb-3">OFFERINGS & FACILITIES</h1>
	<div class="row">
	<div class="col-md-8">

<table cellpadding="22px" style="">
	
	<tbody>
		<tr>
			<td><img src="images/group-exercise-50x50.png" alt=""><span>Group Exercise</span></td>
			<td><img src="images/gym-floor-50x50.png" alt=""><span>GYM Floor</span></td>
			<td><img src="images/cardio-theatre-50x50.png" alt=""><span>Cardio Area</span></td>
			<td class="not-on-mobile"><img src="images/cycle-studio-50x50.png" alt=""><span>Cycle Studio</span></td>
		</tr>
		<tr>
			<td><img src="images/free-weights-50x50.png" alt=""><span>Free Weights</span></td>
			<td><img src="images/freestyle-50x50.png" alt=""><span>Freestyle</span></td>
			<td><img src="images/wifi-50x50.png" alt=""><span>Internet Station</span></td>
			<td class="not-on-mobile"><img src="images/monsoon-shower-50x50.png" alt=""><span>Shower Area</span></td>
		</tr>
		<tr>
			<td><img src="images/steam-sauna-50x50.png" alt=""><span>Steam Room</span></td>
			<td><img src="images/loaded-weights-50x50.png" alt=""><span>Strength Area</span></td>
			<td><img src="images/towel-services-50x50.png" alt=""><span>Towel services</span></td>
			<td class="not-on-mobile"><img src="images/f-b-50x50.png" alt=""><span>F&B</span></td>
		</tr>
		<tr>
			<td><img src="images/pt-50x50.png" alt=""><span>Personal Trainning</span></td>
			<td><img src="images/lounge-area-50x50.png" alt=""><span>Members Lounge</span></td>
			<td><img src="images/yoga-50x50.png" alt=""><span>Yoga </span></td>
			<td class="not-on-mobile"><img src="images/nutrition-counseling-50x50.png"  alt=""><span>Nutrition Counseling</span></td>
		</tr>
	</tbody>
</table>
</div>
<div class="col-md-4" >
	<img class=" rounded-circle img-fluid" src="images/blog2.png">
</div>
</div>
</div>
<!-- OFFERINGS & FACILITIES ended -->
<div class="text-center text-white mb-3 p-4">
	<h1>READY TO MOVE AHEAD WITH US?</h1>
	<a href="franchise_user.php" class="btn btnall">OWN A FRANCHISE</a>
</div>

</div>
	<?php
		include('footer.php');
	?>
</body>
</html>