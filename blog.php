<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<style>
		/* [1] The container */
.img-hover-zoom {
  height: auto; /* [1.1] Set it as per your need */
  overflow: hidden; /* [1.2] Hide the overflowing of child elements */
}

/* [2] Transition property for smooth transformation of images */
.img-hover-zoom img {
  transition: transform .5s ease;
}

/* [3] Finally, transforming the image when container gets hovered */
.img-hover-zoom:hover img {
  transform: scale(1.5);
}

	</style>
</head>
<body style="background-image: linear-gradient(to right , white, #ffffaa);font-family: poppins">
	
<!-- inserting database connection file & getting table from database -->
<?php
include('db/db_setup.php');
$data = get_all_data_from_table('blogs');

?>
<?php
include('nav_header.php');
?>
<div class="container mt-5 text-center">
	<br><br><br>
	<h1 style="align:center;color:gold">BLOGS</h1>
</div>

<?php

for ($i=0; $i <count($data) ; $i++) { 
	
?>

<div class="container mt-4 mb-5 p-2 " ">
<div class="row">

<div style="" class="col-md-2 img-hover-zoom">
<a style="text-decoration: none;color: black; " href="post.php?id=<?php echo $data[$i]['id']; ?>">
<img  src="<?php echo $data[$i]['image']; ?>" style="width:100px;height:100px" class="img img-fluid" alt=""></a>
</div>

<div class="col-md-9 ">
<h3><a style="text-decoration: none;color: black; " href="post.php?id=<?php echo $data[$i]['id']; ?>"><?php echo $data[$i]['title']; ?></a></h3>
<b><?php echo $data[$i]['description']; ?></b>
<address class="font-weight-bold">-
<?php echo $data[$i]['author']; ?>	
</address>
</div>
</div>
</div>


<?php 
}

include('footer.php');
include('scripts.php');

?>
</body>
</html>