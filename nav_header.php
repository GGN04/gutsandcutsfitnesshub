<!DOCTYPE HTML>
<html>
<head>
  <meta content="width=device-width, initial-scale=1" name="viewport" />
<link rel="stylesheet" type="text/css" href="css/style.css">

 <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

</head>
<!-- navbar started -->
<body>
<div class="headc">
  <a href="index.php" class="logo">Guts&Cuts</a>
  <div class="menu-toggle "></div>
  <nav class="navs">
        <ul class="">
          <li class="">
            <a class="active" href="index.php">Home</a>
          </li>
          <li class="">
            <a class="" href="career.php">Careers</a>
          </li>
          <li class="">
            <a class="" href="findus.php">Find us</a>
          </li>
          <li class="">
            <a class=" " href="franchise_own.php" >Own a Franchise</a>
          </li>
          <li class="">
            <a class="" href="contact.php">Contact</a>
          <li class="">
            <a class=" " href="blog.php" >Blogs</a>
          </li>
          <li class="">
            <a class="" href="payu/store.php">Store</a>
          </li>
          
           <li class="">
            <a class="btn btn-info " href="signup.php" >Signup</a>
          </li>
          </li>
      </ul>
</nav>
<div class="clearfix"></div>
</div>
</div>

<!-- navbar ended -->

<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  ></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.menu-toggle').click(function(){
      $('.menu-toggle').toggleClass('active')
      $('nav').toggleClass('active')
    })
  })
</script>

</body>
</html>