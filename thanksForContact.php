<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Thanks For Contact us</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<?php
	include('db/db_setup.php');
	include('nav_header.php');
	include('scripts.php')
	?>
	<?php
		$club = $_POST['club'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $add = $_POST['add'];

        $query = "INSERT INTO `contacts`(`club`,`name`, `email`, `phone`, `message`) VALUES ('".$club."','".$name."','".$email."','".$phone."','".$add."')";
		if (mysqli_query($conn, $query)) {
    		// echo "New record created successfully";
		} else {
    		echo "Error: " . $query . "<br>" . mysqli_error($conn);
		}

		mysqli_close($conn);
	?>


	<div class="text-center bg-dark text-warning pd-5">
		<h1>Thank You</h1>
	</div>
	<div class=" text-center  bg-dark text-warning pb-4 pl-4 pr-4">
		<h3>YOU HAVE GOT A FREE GUEST PASS <b>"<?php echo $name; ?>"</b></h3>
		<br><br>
		<p>Your Free Guest Pass is ready</p>
		<br><br>
		<b>Now you just need to do the following:</b>
		<br><br>
		<p>Go to your inbox where you will find your free guest pass. Take a printout of the email and contact the club you selected to arrange<br> a visit. Alternatively, our club representative will get in touch to help you get started.</p>
		<br><br>
		<b>Enjoy your day at the club!</b>
	</div>
	<?php
	include('footer.php');
	?>
</body>
</html>