<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Home</title>
	<link rel="stylesheet" href="\Guts And Cuts\css\style.css">

<!-- Scripts started -->
<?php 
include('scripts.php');
?>

<!-- Scripts ended -->

<style type="text/css" media="screen">
.img-container {
  position: relative;
  text-align: center;
  color: white;
}

.bottom-left {
  position: absolute;
  bottom: 8px;
  left: 16px;
}

.top-left {
  position: absolute;
  top: 8px;
  left: 16px;
}

.top-right {
  position: absolute;
  top: 8px;
  right: 16px;
}

.bottom-right {
  position: absolute;
  bottom: 8px;
  right: 16px;
}

.centered {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

.img_height
{
  height: 200px;
  width:100%;
}

p{

font-family:impact;  
}
h2
{
  font-family: impact;
}
/* Container holding the image and the text */
.cotainerStyle
{
  position: relative;
}
.box{
  position:relative;
}

.img-title{
  color: black;
  text-align: center;
  background: transparent;
  left: 3%;
  right: 3%;
  bottom: 2%;
  letter-spacing: 3px;
}
.img-t{
  position: absolute;

  color: ;
  text-align: center;
  left: 0;
  right: 0;
  font-size: 50px;
  top: 30%;
  font-family: impact;
  letter-spacing: 15px;
}
.img-para{
	font-size: 15px;
	letter-spacing: 10px;
}
.blur {filter: blur(4px);}
.font-link{
	font-size: 20px;
}
header {
  position: relative;
  background-color: black;
  height: 75vh;
  min-height: 25rem;
  width: 100%;
  overflow: hidden;
}

header video {
  position: absolute;
  top: 50%;
  left: 50%;
  min-width: 100%;
  min-height: 100%;
  width: auto;
  height: auto;
  z-index: 0;
  -ms-transform: translateX(-50%) translateY(-50%);
  -moz-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translateX(-50%) translateY(-50%);
  transform: translateX(-50%) translateY(-50%);
}

header .container {
  position: relative;
  z-index: 2;

}

header .overlay {

  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: black;
  opacity: 0;
  z-index: 1;
}

.display-in-phone{
  display: none;
}
@media (max-width:1200px){
  .overlay{
    display: none;
  }
  header{
    display: none;
  }
  .display-in-phone{
    display: block;
  }
}
@media (max-width:1600px){
  .centered h2{
    font-size: 70px;
    padding-bottom: 12px;
  }
  .centered p{
    font-size: 30px
  }
}
.not-on-mobile{
  display: block;
}
@media (max-width:925px){
  .centered h2{
    font-size: 12px;
  }
  .centered p{
    font-size: 10px;
  }
  .not-on-mobile{
    display: none;
  }
}
@media (max-width:335px){
  .centered h2{
    font-size: 5px;
  }
  .centered p{
    font-size: 5px;
  }
}

</style>

  
</head>
<body  style="font-family:poppins">
<?php
include('nav_header.php');
?>

<!-- display logo in mobile starts -->
<div class="display-in-phone"> 
  <center>
<img src="images/home-flex.jpg"class="img-fluid text-center"  alt="Gymlogo"> 
</center>
</div>
<!-- display logo in mobile ended -->
 <header class="col-md-12 col-sm-12" >
  <div class="overlay "></div>
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="video/bgv_Trim.mp4" type="video/mp4">
  </video>
</header>

<script>

    function typeEffect(element, speed){
      var text = element.innerHTML;
      element.innerHTML = "";

      var i = 0;
      var timer = setInterval(function(){
        if(i < text.length){
          element.append(text.charAt(i));
          i++;
        } else{
          clearINterval(timer);
        }
      }, speed);

    }

      var speed = 75;
      var h1 = document.querySelector('h1');
      var p = document.querySelector('p');
      var delay = h1.innerHTML.length * speed + speed;

      typeEffect(h1, speed);

      setTimeout(function(){
        p.style.display = "inline-block";
        typeEffect(p, speed);
      }, delay);

  </script>


<hr style="border-width: 2px;border-color: gold">



<div class="img-container">
  <img src="images/class.jpg" class="img-fluid"  style="opacity: 0.6" alt="Snow" style="width:100%;">

  <div class="centered">
    <h2>FUNCTIONAL HIIT</h2>
      <p>G&C Training is a global fitness community specializing in innovative, high-intensity group workouts that are fast, fun, and results-driven.</p>
  </div>
</div>

<hr style="border-width: 2px;border-color: gold">

<div class="img-container">
  <img src="images/teamtraning2.jpg" class="img-fluid"  style="opacity: 0.6" alt="Snow" style="width:100%;">

  <div class="centered">
    <h2>TEAM TRAINING</h2>
      <p>The team mentality helps members transform their lifestyle physically and mentally while encouraging community growth and a no-ego attitude</p>
  </div>
</div>

<hr style="border-width: 2px;border-color: gold">

 <div class="container-fluid " style="background-color: black">
  <div class="img-hover-zoom">
    <img class="not-on-mobile img-fluid " style="width: 100%" src="images\bg1.jpg" alt="header image" />
  </div>
  <div class="img-title shadow-lg p-4 pb-2 rounded bg-warning" >
    <h3 style="font-family: impact;letter-spacing: 0px">What is Guts and Cuts</h3>
		<p><b style="letter-spacing: 0px">G&C is one of the most time-efficient ways of training. We aim to burn up to 750 calories per 45-minute session.</b></p>
  </div>




<hr style="border-width: 2px;border-color: gold">
</div>
<!-- footer -->
<?php
include('footer.php');
 ?>
<!-- Footer Ended -->
</body>
</html>