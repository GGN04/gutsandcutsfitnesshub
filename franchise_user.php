<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Registration</title>
	
 	<link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="">
	
	<?php
		include('scripts.php');
	?>
	
<style>
.btnall
{
	background: linear-gradient(90deg,gold,orange);
	font-family: impact;
	font-size: 25px;
	box-shadow: 2px 2px 2px;
}

.btnall:hover
{
	box-shadow: 2px 2px 4px;
	letter-spacing: 1px;
	font-size: 26px;
	background: linear-gradient(100deg,orange,gold);
	transition: 1s;
}
		
</style>
</head>
<body>
	<?php
		include("nav_header.php");
	?>
	<br>
	<div class="container text-warning" style="background-image: linear-gradient(to top, grey,black) ;font-family: poppins">
		<h2 class="text-center mt-2" style="font-family: impact">Thank you for showing interest. Please answer few questions<br> for us, and we shall get back.</h2>
	<center>
	<form class="form m-3 p-3" action="franchise_db_thanks.php" method="post" >
		Your Name ?
		<input type="text" class="form-control in" name="name" placeholder="eg.John" autocomplete="off" required><br>
		Your Phone Number ?
		<input type="number" class="form-control in " name="phone" placeholder="eg.08734936721" autocomplete="off" required><br>
		Your Email Address ? 
		<input type="email" class="form-control in" name="email" placeholder="somebody@gmail.com" autocomplete="off" required><br>

		Capital Available For Investment ? 
		<select name="capital" class="form-control in" >
			<option value="INR 5 Cr To 10 Cr">INR 5 Cr To 10 Cr</option>
			<option value="More than INR 10 Cr">More than INR 10 Cr</option>
		</select><br>

		In which city / cities are you interested to open the Franchise?
		<select name="country" class="form-control in" >
			<option value="Mumbai">Mumbai</option>
			<option value="Delhi">Delhi</option>
			<option value="Hyderabad">Hyderabad</option>
			<option value="Chennai">Chennai</option>
			<option value="Other">Other</option>
		</select><br>
		<input type="submit" name="submit" class="form-control btnall" value="Submit">
	</form>
</center>
		</div>
</body>
</html>