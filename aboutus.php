<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>About Us</title>
	<link rel="stylesheet" href="">
   
   
  <style>
    .btnall
      {
        width: 300px;
        border: 0px solid;
        background: linear-gradient(90deg,gold,orange);
        font-family: impact;
        box-shadow: 2px 2px 2px;
        text-decoration: none;
        color: black
      }
    .btnall:hover
    {
      box-shadow: 2px 2px 4px;
      letter-spacing: 1px;
      font-size: 17px;
      background: linear-gradient(100deg,orange,gold);
      transition: 0.1s;
      text-decoration: none;
      color: black
    }
   .paralax
    {
      height: 600px;
      padding: 100px 140px;
      background: transparent;
    }
    .info{
      margin-top: 400px;
      z-index: 2;
      position: relative;

    }
  </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

  <script src="parallax.js"></script>
  <script src="parallax.min.js"></script>
</head>
<body style="font-family:poppins">
  <?php
include('nav_header.php');
?>


<!-- navbar ended -->
<!-- <div class="contianer">
  <img src="images\bg2.jpg" class="w-100 h-50"  alt="gym_photo">
</div> -->


</div>
    
    <div class="paralax" data-z-index="1" data-parallax="scroll" data-image-src="images/bg2.jpg">
     
     <h1 style="font-family: impact;background-image: linear-gradient(to left bottom,white,grey)" class="text-center info">About Guts & Cuts</h1>

  </div>
  <div class="container-fluid" style="background-image: linear-gradient(to left bottom,gold,#FFDF00)">

<section>
<h2 class="text-center bg-dark p-2" style="color:#FFD700;font-family: impact">We Keep Getting Stronger</h2>  
</section>
<div class="row " style="background-image: linear-gradient(to left bottom,gold,#FFDF00)">

  <div class="col-lg-6 col-md-12">
    <!-- <div class="paralax" data-z-index="1" data-parallax="scroll" data-image-src="images/bg2.jpg"> -->
    <b class="info">The India chapter of G&C's Gym started in 2002, when the first G&C's Gym India branch was set up in Mumbai. In the next few years this number grew, and today G&C's Gym has cut out for itself 120 gyms in India with another 5 under construction and ready to start in this year.</b><br>
    <br>
    <b class="info">With multiple awards for excellence under its belt including 2 awards from the Lions Club for the “Best Fitness Chain”, An award from the Images Beauty & wellness for the Most admired fitness chain of the year”, an award from Franchise India, 8 awards from G&C's Gym International making  it a very eventful 14 years in the growth of the business.<br> G&C's Gym India has bagged the most prestigious Economic Times ET award for ‘Excellence in Marketing 2013’ & ‘Excellence in Customer Service 2014’.</b>
  </div>
<!-- </div> -->
  <div class="col-lg-6 col-md-12">
    <!-- <div class="paralax" data-z-index="1" data-parallax="scroll" data-image-src="images/bg2.jpg"> -->
    <b class="info">G&C's Gym India is acknowledged for its unrivalled success in providing the finest equipment and fitness knowledge available to help its members achieve their individual potential. It follows a globally proven fitness training module with state-of-the-art infrastructure and delivery methodology and continuous up gradation through training programs. With certified trainers and nutritional counseling, G&C's Gym provides a comprehensive approach to the health and well-being of its member.</b><br>
    <br>
    <b class="info">
      Catering to famous celebrities including film & television personalities, sportspersons & Corporate honchos, G&C's Gym lives up to its reputation to give results. Whether your goal is to burn fat, tone or add muscle, build strength, increase flexibility or improve your cardiovascular health, only G&C's Gym has the atmosphere and experience you need.
    </b>
  </div>
</div>
<!-- </div> -->

<div class="row p-4 text-warning text-center"  style="background-image: linear-gradient(to left top,black,grey)">
  <div class="col-lg-6 col-md-12">
    <h1>HOW CAN WE HELP?</h1>
    <p style="padding:20px">We want to help our members go further, so if you have any suggestions about our clubs, classes, personal training sessions or anything else, please come in and talk to one of our team member or if you prefer, drop us a note via our online enquiry forms.</p>
    <a href="contact.php" class="btn btnall">Contact US</a>
  </div>
  <div class="col-lg-6 col-md-12">
    <h1>WORK WITH US</h1>
    <p>We are an ambitious company and we’re always looking for great people to join our team – from personal trainers, group exercise instructors and club managers to our front-of-house teams. We’re committed to training, so you’ll be encouraged to improve your existing skills while you develop new ones.
    <br><br>
    Find out more about becoming part of the Fitness First team. </p>
    <a href="career.php" class="btn btnall">Find out more</a>
  </div>
</div>

</div>

<!-- FOOTER -->
<?php
include('footer.php');
?>
<!-- FOOTER ENDED -->


<!-- Links -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js">
<!-- BootStrap JQuery plugin -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>


</body>
</html>