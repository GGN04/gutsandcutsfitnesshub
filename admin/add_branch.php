<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Branch</title>
	<link rel="stylesheet" href="">
	<?php
	include('../db/db_setup.php');
	include('../scripts.php');
	?>
	<script src="../ckeditor/ckeditor.js"></script>
</head>
<body style="background-color: white">
	<div class="container bg-white mt-4 mb-4">
		<h1 class="text-center">
	<span class="float-left"><a href="dashboard_branch.php" class="btn bg-danger text-white">back</a></span>
		ADD NEW BRANCH</h1>
		
		<?php

		if (isset($_SESSION['success'])) {
			foreach($_SESSION['success'] as $success){
		echo "<font color='green'><b>".$success."</b></font><br>";
		}
		session_destroy();
		}elseif (isset($_SESSION['fail'])) {
			foreach($_SESSION['fail'] as $fail){
		echo "<font color='red'><b>".$fail."</b></font><br>";
		}
		session_destroy();
		}

		?>

		<hr>
		<form name="f1" action="submit_branch.php" method="post" enctype="multipart/form-data">

		<label>Enter Branch Name</label>
			<input type="text" class="form-control border-top-0 border-dark" name="bname" value="">
			<br>
		<label>Enter State Name</label>
			<input type="text" class="form-control border-top-0 border-dark" name="state" value="">
			<br>
			
			
			<label>Enter City Name</label>
			<input type="text" class="form-control border-top-0 border-dark" name="city" value="">
			<br>
			<label>Enter Email</label>
			<input type="text" class="form-control border-top-0 border-dark" name="email" value="">
			<br>
			<label>Enter Contact Number</label>
			<input type="text" class="form-control border-top-0 border-dark" name="contact" value="">
			<br>
			<label>Enter Map tag</label>
			<input type="text" class="form-control border-top-0 border-dark" name="map" value="">
			<br>

			<label>Enter Address</label>
			<textarea rows="10" id="editor1" cols="60" class="form-control border-top-0 border-dark" name="add" ></textarea>
			<br>

		
		<input type="submit" name="" value="Submit Post" class="btn btn-primary btn-lg">
		</form>
	</div>
	           <!--  <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
            </script> -->
</body>
</html>