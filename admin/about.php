<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>About</title>
  <link rel="stylesheet" href="">
  <link rel="stylesheet" href="">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js">
  <style>
    .alink{
      
      text-align: center;
      padding: 14px 25px;
      color: black;
      text-decoration: none;
      display: inline-block;
      font-family: verdana;
    }
    .alink:hover{
      color: black;
      padding:14px 25px;
      border-left: 0px ;
      text-decoration: none;
    }
    .btn-hover:hover{
      background-color: gold;
    }
    
    
    #content{
      margin-top: 60px;
    }
    #about_active{
      background-color: gold;
    }

    .num{
      font-size: 40px;
      margin: 20px 0;
      font-family: arial;
    }

    

  </style>
</head>
<body style="background-color: white;font-family: verdana;">
  <?php 

  include('../db/db_setup.php');
  

  ?>
  <div class="container-fluid ">
  
<?php include('sidebar.php'); ?>
 
  

<div class="float-right col-lg-10 col-md-12 col-sm-12 bg-light p-4 text-center" style="margin-top: 150px;border-radius: 12px">
  <span class="text-info">Owner</span> & <span class="text-success">Developer</span><h3 style="font-family: forte"> Gopal Ganesh Nipane</h3><br>
  
  <h5 class="fixed-bottom"><h1 style="font-family: impact">Guts & Cuts Fitness Hub</h1><img src="../images/copyright.png" style="width: 20px" class="img-fluid" alt="copyright" />opyright 2020-2022</h5>
</div>

</div>
</body>
</html>