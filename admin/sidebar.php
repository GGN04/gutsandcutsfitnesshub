<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
body {
  margin: 0;
  font-family: "Lato", sans-serif;
}

.sidebar {
  margin-top: 50px;
  left: 0;
  padding: 0;
  width: 200px;
  background-color: #f1f1f1;
  position: fixed;
  height: 100%;
  overflow: auto;
}

.sidebar a {
  display: block;
  color: black;
  padding: 16px;
  text-decoration: none;
}
 
.sidebar a.active {
  background-color: gold;
  color: white;
}

.sidebar a:hover:not(.active) {
  background-color: #555;
  color: white;
  cursor: pointer;
}
@media screen and (max-width: 990px) {
  .sidebar {
    width: 100%;
    height: auto;
    position: relative;
  }
  .sidebar a {float: left;}
 

}

@media screen and (max-width: 400px) {
  .sidebar a {
    text-align: center;
    float: none;
  }
}
.alink{
      
      text-align: center;
      padding: 14px 25px;
      color: black;
      text-decoration: none;
      display: inline-block;
      font-family: verdana;
    }
    .alink:hover{
      color: black;
      padding:14px 25px;
      border-left: 0px ;
      text-decoration: none;
    }
.headerdiv{
      background-color: gold;
      margin-top: 0px;
      padding: 1px 0px;

    }
    @media screen and (max-width: 990px) {
  #hide {
   display: none;
  }
  .in-center{
    text-align: center;

  }
}

@media screen and (max-width: 400px) {
  .headerdiv {
    display: none;
  }
}

</style>
</head>
<body>
<div class="row bg-dark fixed-top">
    <div id="hide" class="col-lg-10">
     <h2 class="text-warning in-center pl-2">Guts & Cuts Admin</h2>
    </div>

  <div  class=" col-lg-2 text-center">
  
  <a href="messages.php" class="alink" id="messages_active"><img src="../images/mail.png" /> </a>
  <?php include('user_dropdown.php');
   ?>

  </div>
</div>
<div class="sidebar" >
  <a class="" id="dash_active" href="welcome.php">Dashboard</a>
  <a href="store_dash.php" id="store_active">Store</a>
  <a href="visitors.php" id="visit_active">Visitors</a>
  <a href="about.php" id="about_active">About</a>
</div>


</body>
</html>
