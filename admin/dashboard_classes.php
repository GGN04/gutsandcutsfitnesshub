<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
<?php
include('../db/db_setup.php');
include('../scripts.php');
 if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location:index.php');
    exit;
}
$data = get_all_data_from_table('classes');
?>	
</head>
<body>
	<div class="bg-warning">
	<div class="container-fluid">
		<h1 class="text-center">CLASS DASHBOARD</h1>
		<a href="welcome.php" class="btn bg-danger ">back</a>
		<hr>
		<!-- 
		<?php

		if (isset($_SESSION['success'])) {
			foreach($_SESSION['success'] as $success){
		echo "<font color='green'><b>".$success."</b></font><br>";
		}
		session_destroy();
		}elseif (isset($_SESSION['fail'])) {
			foreach($_SESSION['fail'] as $fail){
		echo "<font color='red'><b>".$fail."</b></font><br>";
		}
		session_destroy();
		}

		?>
		 -->
		<table class="table table-striped">
		<tr>
			<th>Index </th>
			<th>Title</th>
			<th>Option</th>
		</tr>
		
		


		<?php
			for ($i=0; $i <count($data) ; $i++) { 
				$num = $i+1;
				echo "<tr>";
				echo "<td><b>".$num."</b></td>";
				echo "<td><b>".$data[$i]['time']."</b></td>";
				echo "<td> ";
				echo "<a href='edit_classes.php?id=".$data[$i]['id']."'><div class='btn btn-success'> Edit </div></a>";
				echo "<a href='class_opr.php?id=".$data[$i]['id']."&opr=del '><div class='btn btn-success m-2'> Delete </div></a>";
				echo "</td>";
				echo "</tr>";
			}
		?>


</table>
<!-- <pre>
	<?php 
		print_r($data);
	?>
	</pre> -->
</div>
</div>
</body>
</html>