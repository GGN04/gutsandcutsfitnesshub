<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Branch</title>
	<link rel="stylesheet" href="">
	<?php
	include('../db/db_setup.php');
	include('../scripts.php');
	?>
	
</head>
<body style="background-color: white">
	<div class="container bg-white mt-4 mb-4">
		<h1 class="text-center">
	<span class="float-left"><a href="manage_admin.php" class="btn bg-danger text-white">back</a></span>
		ADD NEW ADMIN PROFILE</h1>
		
		<?php

		if (isset($_SESSION['success'])) {
			foreach($_SESSION['success'] as $success){
		echo "<font color='green'><b>".$success."</b></font><br>";
		}
		session_destroy();
		}elseif (isset($_SESSION['fail'])) {
			foreach($_SESSION['fail'] as $fail){
		echo "<font color='red'><b>".$fail."</b></font><br>";
		}
		session_destroy();
		}

		?>

		<hr>
		<form name="f1" action="submit_new_admin.php" method="post" enctype="multipart/form-data">

		<label>Enter Username</label>
			<input type="text" class="form-control border-top-0 border-dark" name="username" value="">
			<br>
		<label>Enter Password</label>
			<input type="text" class="form-control border-top-0 border-dark" name="password" value="">
			<br>	
		<input type="submit" name="" value="Submit" class="btn btn-primary btn-lg">
		</form>
	</div>
	           
</body>
</html>