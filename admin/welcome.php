<?php
if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location:index.php');
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard - Admin</title>
  <link rel="stylesheet" href="">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js">
  <style>
    .alink{
      
      text-align: center;
      padding: 14px 25px;
      color: black;
      text-decoration: none;
      display: inline-block;
      font-family: verdana;
    }
    .alink:hover{
      color: black;
      padding:14px 25px;
      border-left: 0px ;
      text-decoration: none;
    }
    .btn-hover:hover{
      background-color: gold;
    }
    
    
    #content{
      margin-top: 60px;
    }
    #dash_active{
      background-color: gold;
    }

.num{
  font-size: 40px;
  margin: 20px 0;
  font-family: arial;
}

    

  </style>
  
</head>
<body style="background-color: white;font-family: verdana;">
  <?php 
  
 
  include('../db/db_setup.php');
  $user_data = get_all_data_from_table('user');
  $joiner_data = get_all_data_from_table('joiners');
  $franch_investor = get_all_data_from_table('franchise_investors');
  ?>
  <div class="container-fluid">
  
<?php include('sidebar.php'); ?>
 
  

    <div class="float-right col-lg-10 col-md-12 col-sm-12 bg-light " style="margin-top: 10px;border-top-left-radius: 12px;border-top-right-radius: 12px" >
      
      <div class="bg-white"  id="content">
      <h2 class="text-center text-primary">Manage</h2>
      </div>
        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
      <a href="dashboard.php" class="btn btn-hover"><img src="../images/blog.gif" style="width:  300px;height: 160px;background-size: cover;" class="img-fluid mb-2 " alt=""><br>Blogs</a>
    </div>
    <div class="row bg-light text-center">
    <div class="col-lg-6 col-md-12 col-sm-12">
      <a href="dashboard_classes.php" class="btn btn-hover"> <img src="../images/class.gif" style="width:  300px;height: 160px;background-size: cover;" class="img-fluid mb-2" alt=""><br>Classes</a>
    </div>
     <div class="col-lg-6 col-md-12">
      <a href="dashboard_branch.php" class="btn btn-hover"> <img src="../images/branch2.gif" style="width:  300px;height: 160px;background-size: cover;" class="img-fluid mb-2" alt=""><br>Branches</a>
    </div>
      </div>

    </div>
    
</div>


</body>
</html>