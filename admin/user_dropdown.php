<!DOCTYPE html>
<html>
<head>
<style>
.dropbtn {
  background-color: transparent;
  color: black;
  padding: 12px;
  border: none;
  cursor: pointer;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #FDCA1D;
  min-width: 13px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  font-size: 15px;
  padding: 15px 22px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {
  background-color: grey;
  color: white;
}

.dropdown:hover .dropdown-content {
  display: block;
}
.dropbtn:hover{
  padding: 12px;

}
.dropdown:hover .dropbtn {
}
</style>
</head>
<body>


<div class="dropdown">
  <button class="dropbtn fas "><img src="../images/user.png" alt="user"></button>
  <div class="dropdown-content">
  <a href="manage_admin.php">
    <i class="fas "><img src="../images/gear.png" alt=""></i>
    
  </a>

  <a href="logout.php">
  <i class="fas "><img src="../images/logout.png" alt=""></i>
 
  </a>
  </div>
</div>


</body>
</html>
