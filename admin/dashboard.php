<?php
include('../db/db_setup.php');
include('../scripts.php');
//  if(!isset($_SERVER['HTTP_REFERER'])){
//     // redirect them to your desired location
//     header('location:index.php');
//     exit;
// }
$data = get_all_data_from_table('blogs');
?>
<html>
<head>
	
	<title>Blog Dashboard</title>
	<link rel="stylesheet" href="">

</head>
<body style="background-color: white">
	<div class="bg-white">
		
		
		<!-- 
		<?php

		if (isset($_SESSION['success'])) {
			foreach($_SESSION['success'] as $success){
		echo "<font color='green'><b>".$success."</b></font><br>";
		}
		session_destroy();
		}elseif (isset($_SESSION['fail'])) {
			foreach($_SESSION['fail'] as $fail){
		echo "<font color='red'><b>".$fail."</b></font><br>";
		}
		session_destroy();
		}

		?>
		 -->
		 
		 <div class="container-fluid mt-5">
		 	<h1 class="text-center">BLOG DASHBOARD
		 		<span class="float-left">

					<a href="welcome.php" class="btn bg-danger text-white">back</a>
				</span>
				<span class="float-right">
					<a href="new_post.php" class="btn btn-primary">Add New Post</a>
				</span>
		</h1>
		<hr>
		<table class="table text-center">
		<tr>
			<th>Index </th>
			<th>Author</th>
			<th>Title</th>
			<th>Date</th>
			<th>Manage</th>
		</tr>
		
		


		<?php
			for ($i=0; $i <count($data) ; $i++) { 
				$num = $i+1;
				echo "<tr>";
				echo "<td><b>".$num."</b></td>";
				echo "<td><b>".$data[$i]['author']."</b></td>";
				echo "<td><b>".$data[$i]['title']."</b></td>";
				echo "<td><b>".$data[$i]['date']."</b></td>";
				
				echo "<td> ";
				echo "<a href='edit_post.php?id=".$data[$i]['id']."'><div class='btn btn-success'> Edit </div></a>";
				echo "<a href='post_opr.php?id=".$data[$i]['id']."&opr=del '><div class='btn btn-success m-2'> Delete </div></a>";
				echo "</td>";
				echo "</tr>";
			}
		?>


</table>

</div>
<!-- <pre>
	<?php 
		print_r($data);
	?>
	</pre> -->

</div>
</body>
</html>