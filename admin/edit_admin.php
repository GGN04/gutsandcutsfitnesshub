<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit Admin</title>
	<link rel="stylesheet" href="">
	<?php
	include('../db/db_setup.php');
	if(isset($_GET['id'])){
	$ref_id= $_GET['id'];
	$data = get_data_by_id('admin',$ref_id);
	//print_r($data);
	}else{
		header('location:manage_admin.php');
	}
	include('../scripts.php');
	?>
	<script src="../ckeditor/ckeditor.js"></script>
</head>
<body>
	<div class="container-fluid bg-light">
		<h1 class="text-center">Modify Admin Profile
		<span class="float-left"><a href="manage_admin.php" class="btn bg-danger text-white">back</a></span></h1><br>

		<?php

		if (isset($_SESSION['success'])) {
			foreach($_SESSION['success'] as $success){
		echo "<font color='green'><b>".$success."</b></font><br>";
		}
		session_destroy();
		}elseif (isset($_SESSION['fail'])) {
			foreach($_SESSION['fail'] as $fail){
		echo "<font color='red'><b>".$fail."</b></font><br>";
		}
		session_destroy();
		}

		?>

		<hr>
		<form name="f1" action="update_admin.php " class="text-center m-4 p-4" method="post" enctype="multipart/form-data">

		<label>Enter new Username</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<input type="text" class="form-control text-center" name="username" value="<?php echo $data['username'] ?>">
			<br>
			<label>Enter new Password</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<input type="text" class="form-control text-center" name="password" value="<?php echo $data['password'] ?>">
			<br>


		
		
		<input type="submit" name="" value="Update Post" class="btn btn-primary btn-lg">
		</form>
	</div>
	            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
            </script>
</body>
</html>