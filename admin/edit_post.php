<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit Post</title>
	<link rel="stylesheet" href="">
	<?php
	include('../db/db_setup.php');
	if(isset($_GET['id'])){
	$ref_id= $_GET['id'];
	$data = get_data_by_id('blogs',$ref_id);
	//print_r($data);
	}else{
		header('location:dashboard.php');
	}
	include('../scripts.php');
	?>
	<script src="../ckeditor/ckeditor.js"></script>
</head>
<body>
	<div class="container-fluid bg-warning">
		<h1 class="text-center">EDIT AN POST
		<span class="float-left"><a href="dashboard.php" class="btn bg-danger text-white">back</a></span></h1><br>

		<?php

		if (isset($_SESSION['success'])) {
			foreach($_SESSION['success'] as $success){
		echo "<font color='green'><b>".$success."</b></font><br>";
		}
		session_destroy();
		}elseif (isset($_SESSION['fail'])) {
			foreach($_SESSION['fail'] as $fail){
		echo "<font color='red'><b>".$fail."</b></font><br>";
		}
		session_destroy();
		}

		?>

		<hr>
		<form name="f1" action="update_post.php" method="post" enctype="multipart/form-data">

		<label>Enter Post Title</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<input type="text" class="form-control" name="title" value="<?php echo $data['title'] ?>">
			<br>
			<label>Enter Author Name</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<input type="text" class="form-control" name="author" value="<?php echo $data['author'] ?>">
			<br>


		<label>Enter Post Description</label>
			<textarea rows="10" id="editor1" cols="60" class="form-control" name="post_data">
				<?php echo $data['description'] ?>
			</textarea>
			<br>
		<img src="../<?php echo $data['image'] ?>" style="max-width: 180px;"><br>
		<label>Select An Image To Upload</label>
			<input type="File" name="image" class="form-control" value="<?php echo $data['image'] ?>">
			<br>
		<input type="submit" name="" value="Update Post" class="btn btn-primary btn-lg">
		</form>
	</div>
	            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
            </script>
</body>
</html>