<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Profile</title>
  <link rel="stylesheet" href="">
  <link rel="stylesheet" href="">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js">

<?php
include('../db/db_setup.php');
include('../scripts.php');
 
$data = get_all_data_from_table('admin');
?>  


  <style>
    .alink{
      
      text-align: center;
      padding: 14px 25px;
      color: black;
      text-decoration: none;
      display: inline-block;
      font-family: verdana;
    }
    .alink:hover{
      color: black;
      padding:14px 25px;
      border-left: 0px ;
      text-decoration: none;
    }
    .btn-hover:hover{
      background-color: gold;
    }
    
    
    #content{
      margin-top: 60px;
    }
    

    .num{
      font-size: 40px;
      margin: 20px 0;
      font-family: arial;
    }
    .pass_hide{
      color: transparent;
    }
    .pass_hide:hover{
      color: black;
    }
    

  </style>
</head>
<body style="background-color: white;font-family: verdana;">
  
  <div class="container-fluid">
  
<?php include('sidebar.php'); ?>
<div class="float-right col-lg-10 col-md-12 col-sm-12 bg-light " style="margin-top: 70px;" >
 <h2 class="text-center text-primary">Admin Profile<span class="float-right"><a href="add_admin_profile.php" class="btn btn-primary ">Add New Admin Profile</a></span></h2>
  <table class="table table-striped text-center">
    <tr>
      <th>Index </th>
      <th>Username</th>
      <th>Password</th>
      <th>Option</th>
    </tr>
 
    <?php
      for ($i=0; $i <count($data) ; $i++) { 
        $num = $i+1;
        echo "<tr>";
        echo "<td><b>".$num."</b></td>";
        echo "<td><b>".$data[$i]['username']."</b></td>";
        echo "<td><b class='pass_hide'>".$data[$i]['password']."</b></td>";
        echo "<td> ";
        echo "<a href='edit_admin.php?id=".$data[$i]['id']."'><div class='btn btn-success'> Edit </div></a>";
        echo "<a href='admin_opr.php?id=".$data[$i]['id']."&opr=del '><div class='btn btn-success m-2'> Delete </div></a>";
        echo "</td>";
        echo "</tr>";
      }
    ?>


</table>
</div>

</div>
</body>
</html>