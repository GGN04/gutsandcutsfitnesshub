<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit Branch</title>
	<link rel="stylesheet" href="">
	<?php
	include('../db/db_setup.php');
	if(isset($_GET['id'])){
	$ref_id= $_GET['id'];
	$data = get_data_by_id('branch',$ref_id);
	//print_r($data);
	}else{
		header('location:dashboard_branch.php');
	}
	include('../scripts.php');
	?>
	<script src="../ckeditor/ckeditor.js"></script>
</head>
<body>
	<div class="container-fluid bg-light">
		<h1 class="text-center">Modify Branch Details
		<span class="float-left"><a href="dashboard_branch.php" class="btn bg-danger text-white">back</a></span></h1><br>

		<?php

		if (isset($_SESSION['success'])) {
			foreach($_SESSION['success'] as $success){
		echo "<font color='green'><b>".$success."</b></font><br>";
		}
		session_destroy();
		}elseif (isset($_SESSION['fail'])) {
			foreach($_SESSION['fail'] as $fail){
		echo "<font color='red'><b>".$fail."</b></font><br>";
		}
		session_destroy();
		}

		?>

		<hr>
		<form name="f1" action="update_branch.php" class="text-center m-4 p-4" method="post" enctype="multipart/form-data">

		<label>Edit Branch Name</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<input type="text" class="form-control text-center" name="bname" value="<?php echo $data['branch_name'] ?>">
			<br>
			<label>Edit State Name</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<input type="text" class="form-control text-center" name="sname" value="<?php echo $data['state'] ?>">
			<br>
			<label>Edit City Name</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<input type="text" class="form-control text-center" name="cname" value="<?php echo $data['city'] ?>">
			<br>
			<label>Edit Address</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<input type="address" class="form-control text-center" name="address" value="<?php echo $data['address'] ?>">
			<br>
			<label>Edit Contact Number</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<input type="text" class="form-control text-center" name="contact" value="<?php echo $data['contact'] ?>">
			<br>
			<label>Edit Email ID</label>
		<input type="hidden" name="beid" value="<?php echo $data['id'] ?>">
			<input type="text" class="form-control text-center" name="bemail" value="<?php echo $data['email'] ?>">
			<br>
			<label>Edit Map URL</label><br>
			Eg.Code<br>
			<code>&lt;iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30156.933438069595!2d72.83810608264129!3d19.1244649691183!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c83c05b7fc89%3A0x324fb542f69a0e2e!2sInfiniti%20Mall!5e0!3m2!1sen!2sin!4v1585733631981!5m2!1sen!2sin" width="1050" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"&gt; &lt;/iframe&gt;  </code>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<input type="text" class="form-control text-center" name="bmap" value="">
			<br>


		
		
		<input type="submit" name="" value="Update Post" class="btn btn-primary btn-lg">
		</form>
	</div>
	            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
            </script>
</body>
</html>