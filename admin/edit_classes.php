<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit Classes</title>
	<link rel="stylesheet" href="">
	<?php
	include('../db/db_setup.php');
	if(isset($_GET['id'])){
	$ref_id= $_GET['id'];
	$data = get_data_by_id('classes',$ref_id);
	//print_r($data);
	}else{
		header('location:dashboard_classes.php');
	}
	include('../scripts.php');
	?>
	<style type="text/css" media="screen">
	.in{
		width: 300px;
	}	
	</style>
</head>
<body>
	<div class="bg-warning p-4">
	<center>
	<h1>EDIT CLASSES</h1>
	<a href="dashboard_classes.php" class="btn bg-danger float-left ">back</a><br>

		<?php

		if (isset($_SESSION['success'])) {
			foreach($_SESSION['success'] as $success){
		echo "<font color='green'><b>".$success."</b></font><br>";
		}
		session_destroy();
		}elseif (isset($_SESSION['fail'])) {
			foreach($_SESSION['fail'] as $fail){
		echo "<font color='red'><b>".$fail."</b></font><br>";
		}
		session_destroy();
		}

		?>
		<hr>

	<form class="form" action="update_classes.php" method="post">
		<label class="">Time</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
		<input class="form-control in" type="text" name="time" placeholder="eg. 7.00-8.00 am" value="<?php echo $data['time'] ?>">
		<lable class="">Monday</lable>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
		<input class="form-control in" type="text" name="monday" placeholder="eg. Yoga/Bodypump" value="<?php echo $data['monday'] ?>">
		<label class="">Tuesday</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
		<input class="form-control in" type="text" name="tuesday" placeholder="eg. Yoga/Bodypump" value="<?php echo $data['tuesday'] ?>">
		<label class="">Wednesday</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
		<input class="form-control in" type="text" name="wednesday" placeholder="eg. Yoga/Bodypump" value="<?php echo $data['wednesday'] ?>">
		<label class="">Thursday</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
		<input class="form-control in" type="text" name="thursday" placeholder="eg. Yoga/Bodypump" value="<?php echo $data['thursday'] ?>">
		<label class="">Friday</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
		<input class="form-control in" type="text" name="friday" placeholder="eg. Yoga/Bodypump" value="<?php echo $data['friday'] ?>">
		<label class="">Saturday</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
		<input class="form-control in" type="text" name="saturday" placeholder="eg. Yoga/Bodypump" value="<?php echo $data['saturday'] ?>">
		<label class="">Sunday</label>
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
		<input class="form-control in" type="text" name="sunday" placeholder="eg. Yoga/Bodypump"><br>
		<input type="submit" class="btn btn-primary">
	</form>
</div>
</center>
</body>
</html>