<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<?php
	include('../db/db_setup.php');
	include('../scripts.php');
	 if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location:index.php');
    exit;
}
	?>
	<script src="../ckeditor/ckeditor.js"></script>
</head>
<body>
	<div class="container bg-white mt-4">
		<h1 class="text-center">MAKE A NEW POST
		<span class="float-left">
		<a href="dashboard.php" class="btn bg-danger text-white">back</a>
		</span>
		</h1>
		<?php

		if (isset($_SESSION['success'])) {
			foreach($_SESSION['success'] as $success){
		echo "<font color='green'><b>".$success."</b></font><br>";
		}
		session_destroy();
		}elseif (isset($_SESSION['fail'])) {
			foreach($_SESSION['fail'] as $fail){
		echo "<font color='red'><b>".$fail."</b></font><br>";
		}
		session_destroy();
		}

		?>

		<hr>
		<form name="f1" action="submit_post.php" method="post" enctype="multipart/form-data">

		<label>Enter Post Title</label>
			<input type="text" class="form-control border-top-0 border-dark" name="title" value="">
			<br>
		<label>Enter Author Name</label>
			<input type="text" class="form-control border-top-0 border-dark" name="author" value="">
			<br>

		<label>Select An Image To Upload</label>
			<input type="File" name="image" class="form-control border-0" value="">
			<br>

		<label>Enter Post Description</label>
			<textarea rows="10" id="editor1" cols="60" class="form-control" name="post_data" ></textarea>
			<br>
			
		<input type="submit" name="" value="Submit Post" class="btn btn-primary btn-lg">
		</form>
	</div>
	            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
            </script>
</body>
</html>