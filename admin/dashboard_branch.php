<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Branch</title>
	<link rel="stylesheet" href="">
<?php
include('../db/db_setup.php');
include('../scripts.php');
 if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location:index.php');
    exit;
}
$data = get_all_data_from_table('branch');
?>	
</head>
<body>
	<div class="container-fluid bg-white mt-5">
		<h1 class="text-center">
			<span class="float-left"><a href="welcome.php" class="btn bg-danger text-white">back</a></span>
			BRANCH DASHBOARD
			<span class="float-right"><a href="add_branch.php" class="btn btn-primary ">Add New Branch</a></span>
		</h1>
		
		<hr>
		<!-- 
		<?php

		if (isset($_SESSION['success'])) {
			foreach($_SESSION['success'] as $success){
		echo "<font color='green'><b>".$success."</b></font><br>";
		}
		session_destroy();
		}elseif (isset($_SESSION['fail'])) {
			foreach($_SESSION['fail'] as $fail){
		echo "<font color='red'><b>".$fail."</b></font><br>";
		}
		session_destroy();
		}

		?>
		 -->
		 <div class="container-fluid">
		<table class="table">
		<tr>
			<th>Index </th>
			<th>Branch Name</th>
			<th>State</th>
			<th>Options</th>
		</tr>
		
		


		<?php
			for ($i=0; $i <count($data) ; $i++) { 
				$num = $i+1;
				echo "<tr>";
				echo "<td><b>".$num."</b></td>";
				echo "<td><b>".$data[$i]['branch_name']."</b></td>";
				echo "<td><b>".$data[$i]['state']."</b></td>";
				echo "<td> ";
				echo "<a href='edit_branch.php?id=".$data[$i]['id']."'><div class='btn btn-success'> Edit </div></a>";
				echo "<a href='branch_opr.php?id=".$data[$i]['id']."&opr=del '><div class='btn btn-success m-2'> Delete </div></a>";
				echo "</td>";
				echo "</tr>";
			}
		?>


</table>
<div>
	
</div>
</div>
<!-- <pre>
	<?php 
		print_r($data);
	?>
	</pre> -->

</div>
</body>
</html>