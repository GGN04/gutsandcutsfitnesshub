<?php 
include('db/db_setup.php') ;


$val = $_GET['id'];
$data = get_data_by_key('id',$val,'blogs');
//print_r($data);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<?php
		include('nav_header.php');
	?>
	<center>
	<div style="margin-top: 100px" class="container ">
	<h1 style="text-transform: uppercase"><?php echo $data[0]['title']; ?></h1>
	
	<hr>
	<div>
	<img src="<?php echo $data[0]['image']; ?>" style="width=300px;height: 300px" class="img img-fluid" alt="">
	</div>
	<br>
	<b><?php echo $data[0]['description']; ?></b>
	<b class="text-success">- <?php echo $data[0]['author']; ?></b>
	</div>
	</center>
	<?php
		include('footer.php');
		include('scripts.php');
	?>
</body>
</html>