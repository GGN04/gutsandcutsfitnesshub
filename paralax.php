<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<style>
		.paralax
		{
			padding: 100px 20px;
			background: transparent;
		}
		.info{
			color: #fff;
			z-index: 2;
			position: relative;
		}
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

	<script src="parallax.js"></script>
	<script src="parallax.min.js"></script>
</head>
<body>
	<div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur. </p>
		<div class="paralax" data-z-index="1" data-parallax="scroll" data-image-src="images/bg2.jpg">
			<h1 class="info" >Guts & Guts</h1>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p>

		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p>
		<div class="paralax" data-parallax="scroll" data-image-src="images/robby.jpg">
			
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p>

		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere minus reprehenderit magnam ut ea maxime magni maiores iusto provident, ipsum harum voluptate atque? Laboriosam alias nulla porro sit minus ut! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem impedit eos atque, eveniet error, cum natus, recusandae velit libero dolores debitis officiis blanditiis deleniti quisquam placeat aut temporibus. Aspernatur.</p>

	</div>
</body>
</html>