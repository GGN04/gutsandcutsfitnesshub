<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Find Us</title>
	<link rel="stylesheet" href="">
	<?php
	include('db/db_setup.php');
	$data = get_all_data_from_table('branch');
	?>
</head>
<body  id="body" style="overflow:hidden;font-family: Poppins">
	<?php
		include('scripts.php');
		include('nav_header.php');
	?>
	<br>
	<p></p> 
<div class=" bg-warning text-center p-4" style="margin-top: 110px;">
		<h1 mt-4> Our Branches Over India</h1>
    <?php
    for ($i=0; $i <count($data) ; $i++) { 
    ?>
		
    <div class="container  bg-warning text-center p-4 m-3">
	<h4 class="float-left" style="text-transform: uppercase"><?php echo $data[$i]['state']; ?></h4>
	<a  type="submit"  href="branch.php?id=<?php echo $data[$i]['id']; ?>" class="btn btn-primary float-right">DETAILS</a>
    </div>
    <?php
    }			
    ?>
</div>
</body>
</html>