
<!-- FOOTER -->
<footer  style="background-color: black;font-family: poppins">
        <div class="container  bg-transparent" style="background:black">
            <div class="row ">
                <div class="col-md-4 text-center text-md-left ">
                    
                    <div class="py-0">
                        <h4 style="font-family: impact" class="my-4 text-white">About<span class="mx-2 text-warning ">Guts & Cuts</span></h4>

                        <p class="footer-links font-weight-bold">
                            <a class="text-white" style="font-family: arial" href="index.php">Home</a>
                            |
                            <a class="text-white link-active" style="font-family: arial" href="blog.php">Blogs</a>
                            |
                            <a class="text-white" style="font-family: arial" href="aboutus.php">Why G&C</a>
                            |
                            <a class="text-white" style="font-family: arial" href="contact.php">Contact</a>
                            |
                            <a class="text-white" style="font-family: arial" href="findus.php">Find us</a>
                            |
                            <a class="text-white" style="font-family: arial" href="franchise_own.php">Own a Franchise</a>
                          </p>
                        <p class="text-light py-3 mb-4" style="font-family: arial">&copy;2020 Guts & Cuts Fitness Hub</p>
                    </div>
                </div>
                
                <div class="col-md-4 text-white text-center text-md-left mb-2 mt-2"> 
                    <img src="images/logo.png" class="img-fluid rounded-circle" alt=""> 
                </div>
                
                <div class="col-md-4 text-white my-4 text-center text-md-left ">
                    <h4 style="font-family: impact"><span class="text-warning ">About the Hub</span></h4>
                        <p style="font-family: arial"><b class=" my-2 text-lowercase" >A MIX OF CIRCUIT AND HIIT STYLE WORKOUTS
                         GEARED TOWARDS EVERYDAY MOVEMENT. G&C IS THE TOTAL AMOUNT OF TIME FOR SWEAT-DRIPPING, HEART-PUMPING FUN.</b></p>
                    <!-- <div class="py-2">
                        <a href="#"><i class="fab fa-facebook fa-2x text-primary mx-3"></i></a>
                        <a href="#"><i class="fab fa-google-plus fa-2x text-danger mx-3"></i></a>
                        <a href="#"><i class="fab fa-twitter fa-2x text-info mx-3"></i></a>
                        <a href="#"><i class="fab fa-youtube fa-2x text-danger mx-3"></i></a>
                    </div> -->
                </div>
            </div>  
        </div>
     </footer>
<!-- FOOTER ENDED -->
