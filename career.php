<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Career</title>
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/details_style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <script src="js/my_js.js"></script>

<style type="text/css" media="screen">

.warning {
  border-color: #ff9800;
  color: orange;
}

.warning:hover {
  background: black;
  color: #FFD700;
  font-size: 15px;
  letter-spacing: 2px;
}
.btnall {
        border: 0px;
        padding: 2px;
        font-size: 20px;
        background: linear-gradient(90deg,gold,orange);
        font-family: impact;
        box-shadow: 2px 2px 2px;
}
.btnall:hover{
      box-shadow: 2px 2px 4px;
      letter-spacing: 1px;
      font-size: 25px;
      background: linear-gradient(100deg,orange,gold);
      transition: 1s;
}
</style>
</head>
<body style="font-family:poppins">
<?php 
  include('nav_header.php');
?>
<!-- header -->
<header>
<div style="font-family: impact;background-image: linear-gradient(to left, gold, orange);" class=" text-center text-black  p-4 ">

	<h1>STEP UP TO A FITTER CAREER</h1>
	<h5>In Asia, we have over 80 clubs in 6 countries with over 210,000 members. We have 7 clubs in India and over 500 employees here, many of whom have grown with us since we started in India.<br><br>

  This growth has been built through the quality of our staff. Our successful mix of recruitment, training, future development and career opportunities enables us to continually exceed our employees’ expectations.<br><br>

  We seek dynamic and passionate personnel to meet our rapid expansion needs. The opportunities for growth are tremendous, with more clubs opening. </h5>
</div>
</header>

<!-- header ended -->

<!-- Requirements -->
<div class="text-center bg-dark text-warning">
  <h1 style="font-family: impact">WORKING WITH US</h1>
</div>
  <section class=" text-dark ">
  <details >
 <summary>Why Fitness First?</summary>
<div>
      <h4><strong>WORLD-CLASS FACILITIES</strong></h4>
<p>Across our clubs, because members enjoy Fitness First like their second home, we strive to keep consistency in the look and feel of all our clubs by maintaining the world-class facilities we offer and the high standard of service.</p>
&nbsp;
<p><strong>Gym</strong></p>
<p>Fitness First specialises in safe cardiovascular exercise programmes to enable you to improve your lifestyle, health and general wellbeing. Exercise is great for weight loss, shaping and toning and is in fact the only way to lose weight in the right places and keep it off! Our spacious, air-conditioned gymnasiums are fully equipped with a comprehensive range of cardio equipment such as treadmills, steppers and ellipticals. At your disposal is also 3 tons of free weights &amp; machine equipment along with a stretch area. Besides this, we offer a ton of encouragement you need to achieve your goals.</p>
&nbsp;
<p><strong>Studio</strong></p>
<p>Group exercise is a very important part of a cardiovascular workout - Our studio programs offer an exciting range of classes including world-renowned Les Mills programs such as BODYCOMBAT™, BODYPUMP™, BODYBALANCE™, BODYVIVE™, RPM™, Yoga, Tai Chi and everything in-between. The studio is built on a fully sprung floor for your safety and the programs have been designed specifically to cater to the needs of all ages and abilities.</p>
&nbsp;
<p><strong>1:1 Personal Training Counter</strong></p>
<p>Personal Training is not just for movie stars! Now you can get affordable personal training to help you achieve your fitness goals. Maximize results with minimum time! For more information on how we can help you achieve your goals, please speak to our Fitness Manager or any of our Personal Trainers.</p>
&nbsp;
<p><strong>Cardio Theater</strong></p>
<p>Exercise is more fun with personalised audio-visual entertainment. Whether you enjoy watching music videos, news or the latest sporting event, the cardio theatre will make that 5 mile run just fly by. Exercise no longer need to be boring with cardio theatre giving you a choice of 5 to 6 Astro channels to keep you entertained whilst you are exercising.</p>
&nbsp;
<p><strong>Cycling Studio</strong></p>
<p>After receiving great feedback, we now have dedicated Cycling studios in many of our clubs. Spinning is a fun and exciting-bike based aerobic workout ideal for all fitness levels. You can burn up to 500 – 600 calories in a class!</p>
&nbsp;
<p><strong>Member's Lounge</strong></p>
<p>After spending an hour or two sweating it out in the gym, the member's lounge is the perfect place to unwind. Relax, read the paper, chat with other members and friends as well as enjoy complimentary tea and coffee in the members’ lounge.</p>
&nbsp;
<p><strong>Luxurious changing Room</strong></p>
<p>With relaxation being an important part of a complete exercise programme, treat yourself to the luxurious changing room with shower facilities and complimentary toiletry, hairdryers, restrooms &amp; personalised lockers.</p>
    </div>
  </details>
  <details>
    <summary>Take your career to the next level</summary>
    <div>
      <p>We give our team the opportunity to get more out of their career by inspiring each other to learn new things, achieve new qualifications, and get fitter together. We recognise talent and support our team to go further in their career. From taking advantage of our global opportunities to having the confidence to inspire our members, we encourage our team to raise the bar so we can celebrate and reward their great results.</p>
    </div>
  </details>
  <details>
    <summary>Helping you go further</summary>
    <div>
      <p>We’re committed to inspiring our members to get fitter as we believe this helps them build confidence to get more out of life.</p>
    </div>
  </details>
   <details>
    <summary>Inspiring and fun environments</summary>
    <div>
      <p>We work together as one team in environments full of inspiration, innovation and fun!</p>
    </div>
  </details>
  <details>
    <summary>The opportunity to lead a fitter lifestyle</summary>
    <div>
      <p>We encourage everyone to work out. It helps us feel great and go further in life. As an employee, you get free membership and a discounted one for a friend/family member.</p>
    </div>
  </details>
   <details>
    <summary>World-class training and development</summary>
    <div>
      <p>Our people count. That’s why we offer ongoing development from the day you join to build a team of fitness experts.</p>
    </div>
  </details>
</section>

<!-- Requirement ended -->

<!-- SIGN UP CONTAINER -->
<div class="container-fluid bg-warning p-5" style="background-image: linear-gradient(to left bottom, black, grey);
  ">
    <h1  style="font-family: impact;color: white;text-transform:  uppercase;" class="text-center">The various career opportunities or positions available </h1>
      <center>
        <form>
    <button type="submit" formaction="join_now.php" class="btnall">Join Us</button>

    </form>
    </center>
</div>
<br>
<!-- SIGN UP CONTAINER ENDED-->

<!-- FOOTER -->
<?php 
  include('footer.php');
?>
<!-- FOOTER ENDED -->

<!-- Bootstrap css plugin -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js">
<!-- BootStrap JQuery plugin -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>


</body>
</html>