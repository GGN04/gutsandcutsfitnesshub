<?php
	include('../scripts.php');
	
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<center>
	<div class="container p-3">
		<h1 class="text-center">Login Form</h1>
		<form method="post" name="myForm" action="log.php" >
			<input type="hidden" name="pnm" value="<?php echo '".$pna."' ?>">
			<input type="hidden" name="prr" value="<?php echo '".$pri."' ?>"> 

			<input type="text" class="form-control" name="cname" id="cname" placeholder="enter your name" />
			<input type="password" class="form-control" name="cpass" id="cpass" placeholder="enter your password" />
			<input type="text" class="form-control" maxlength="10" name="phone" id="phone" placeholder="enter your phone">
			<input type="submit" class="btn btn-primary" >
		</form>
		<script  type = "text/javascript">

      // Form validation code will come here.
      function valiform() {
           
         if( document.myForm.cname.value == "" ) {
            alert( "Please provide your name!" );
            document.myForm.cname.focus() ;
            return false;
         }
          if( document.myForm.cpass.value == "" ) {
            alert( "Please Enter Your Password" );
            document.myForm.cpass.focus() ;
            return false;
         }
          
         // if( document.myForm.email.value == "" ) {
         //    alert( "Please provide your Email!" );
         //    document.myForm.email.focus() ;
         //    return false;
         // }
         if( document.myForm.phone.value == "" || isNaN( document.myForm.phone.value ) ||
            document.myForm.phone.value.length != 10 ) {
            
            alert( "Please provide your phone number and only enter 10 numbers" );
            document.myForm.phone.focus() ;
            return false;
         }

         // var emailID = document.myForm.email.value;
         // atpos = emailID.indexOf("@");
         // dotpos = emailID.lastIndexOf(".");
         
         // if (atpos < 1 || ( dotpos - atpos < 2 )) {
         //    alert("Please enter correct email ID")
         //    document.myForm.email.focus() ;
         //    return false;
         // }
         return( true );
      }



</script>
	</div>
	<?php 

	?>
</center>
</body>
</html>