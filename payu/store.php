<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Store - G&C</title>
	<link rel="stylesheet" href="">
	<?php
	include('../db/db_setup.php');
	include('../scripts.php');
	?>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body class="text-center bg-light">
	<?php
	
	?>
	<div class="container" style="">
		<h1 class=""><span class="float-left"><a href="../index.php" class="btn text-white btn-warning">Go Back</a></span> GUTS & CUTS STORE  <span class="float-right"><a href="viewCart.php" class="btn btn-warning"><img src="cart.png" alt="cart" class=""><b style="display: none;font-size:20px;" class="added">+</b></a></span> </h1>
		
		<div class="row">

		<?php
		

		$query = "SELECT `name`, `image`, `price`,`rate`, `discount` FROM `shopping_cart` order by id asc ";

		$result = mysqli_query($conn, $query);

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			while ($product = mysqli_fetch_array($result)) {
				?>
				
				<div class="col-lg-4 col-md-4 col-sm-12 p-2">
					<form method="post" name="cartform" action="insert.php">
						<div class="card">
							<h6 id="name" name="name" class="card-title bg-info text-white p-2 text-uppercase"> <?php echo $product['name'] ; ?></h6>
							<input type="hidden" id="name" name="name" class="card-title  border-0 text-center bg-info text-white p-2 text-uppercase" value="<?php echo $product['name'] ; ?> " >
								
							<div class="card-body">
								<div class="pb-2 bg-white" style="">
								<img src="<?php echo $product['image'] ; ?>" alt="image" style="width:  110px;height: 100px;background-size: cover;" class=" mb-2">
								<input type="hidden" value="<?php echo $product['image'] ; ?>" name="img">
								</div><br>
								<h6 name="price" id="price"> <strike class="text-danger"> &#8377; <?php echo $product['price'] ; ?> </strike> &nbsp;
								
								<span> &#8377; <?php $temp = $product['price'] / 100 * $product['discount'] ; $dis = $product['price'] - $temp; echo $dis ; ?></span>
								<span> (<?php echo $product['discount'] ; ?>% off) </span></h6>
								<input type="hidden" id="price" name="price" size="2" class="bg-transparent border-0 text-uppercase" value="<?php echo $dis; ?> " >


								<h6 class="badge badge-success"> <?php echo $product['rate'] ; ?> <i class="fa fa-star"></i></h6><br>	
								Quantity
								<input type="number" name="qty" id="qty" class="form-control text-center" value="1" placeholder="Quantity" min="1" >

							</div>
							<div class="btn-group d-flex">
								<button type="submit"  class="btn btn-success text-white call-us" > Add to Cart</button>
								<!-- <button formaction="insert.php" class="btn btn-warning text-white"> Buy Now</button> -->
								<script>
							(function($) {
								  $("button.call-us").click(function() {
								    
								    $("b.added").show();
								  });
								})(jQuery);
							</script>
							</div>
						</div>
					</form>
				</div>


		<?php
			}
		}


		?>
		
</div>

	
</body>
</html>