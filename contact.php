<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Contact</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
<style>
	.warning 
	{
	  border-color: #ff9800;
	  color: orange;
	}

	.warning:hover 
	{
	  background: black;
	  color: #FFD700;
	  font-size: 20px;
	}

	.btnn 
	{
	  border: 0px solid ;
	  background-color: #FFD700;
	  color: black;
	  padding: 14px 28px;
	  font-size: 16px;
	  cursor: pointer;
	}

	.btnall
	{
	  background: linear-gradient(90deg,gold,orange);
	  font-family: impact;
	  font-size: 25px;
	  box-shadow: 2px 2px 2px;
	}
	
	.btnall:hover
	{
	  box-shadow: 2px 2px 4px;
	  letter-spacing: 1px;
	  font-size: 20px;
	  background: linear-gradient(100deg,orange,gold);
	  transition: 1s;
	}

	@media(max-width:635px)
	{
 	  .not-on-mobile
       {
			display: none;
		}
	}

	</style>
		<script  type = "text/javascript">

      // Form validation code will come here.
      function validate() {
      		if( document.myForm.department.value == "-1" ) {
            alert( "Please provide your Department!" );
            return false;
         }
         if( document.myForm.name.value == "" ) {
            alert( "Please provide your name!" );
            document.myForm.name.focus() ;
            return false;
         }
         if( document.myForm.email.value == "" ) {
            alert( "Please provide your Email!" );
            document.myForm.email.focus() ;
            return false;
         }
         if( document.myForm.phone.value == "" || isNaN( document.myForm.phone.value ) ||
            document.myForm.phone.value.length != 10 ) {
            
            alert( "Please provide your phone number and only enter 10 numbers" );
            document.myForm.phone.focus() ;
            return false;
         }
         
         var emailID = document.myForm.email.value;
         atpos = emailID.indexOf("@");
         dotpos = emailID.lastIndexOf(".");
         
         if (atpos < 1 || ( dotpos - atpos < 2 )) {
            alert("Please enter correct email ID")
            document.myForm.email.focus() ;
            return false;
         }
         return( true );
      }



</script>
</head>
<body style="background-color: black;font-family: poppins">
<!-- navbar started -->
<?php 
	include('nav_header.php');
?>
<br><br>
<!-- navbar ended -->

<!-- main container-->
<div class="container-fluid">
<div class="row   text-warning">

	<div  style="background-image: linear-gradient(to left bottom, black, grey);" class="col-lg-6 col-md-12 text-center p-5">
		<br><br><br>
		<h1 style="font-family:impact">CONTACT US NOW</h1>
		<p> WE ARE ALWAYS READY TO HEAR FROM YOU</p>
		<BR>
		<P>OUR TEAM IS ALWAYS READY TO HEAR FROM YOU. WE MAKE IT A POINT TO RESPOND WITHIN 24 HOURS ON WEEKDAYS AND 48 HOURS ON WEEKENDS.</P>
		<p><b>Head Branch:</b>Infinity Mall, New Link Rd, Phase D, Oshiwara, Andheri West, Mumbai, Maharashtra 400053<br>
		Phone: +91 89 76 83 48 31 / 32<br>
		Email: WeCare@Guts&Cuts.in</p>
	</div>
	<div  style="background-image: linear-gradient(to left bottom, gold, skyblue);"  class="not-on-mobile col-lg-6 col-md-12 p-5 ">
	
	<?php
	include('map.php');
	?>
	</div>
	
	
</div>
<div class="container mt-2 p-2">
			<form  action="thanksForContact.php" onsubmit = "return(validate());" name="myForm" method="post" accept-charset="utf-8">
			
				<select class="form-control" name="club" class="bg-dark" required>
				<option value="-1" selected>[SELECT A CLUB]</option>
					<option value="INFINITY MALL | MUMBAI">INFINITY MALL | MUMBAI</option>
					<option value="BEAUTIFULL COMPLEX | DELHI">BEAUTIFULL COMPLEX | DELHI</option>
					<option value="MALL OF SOUTH | CHENNAI">MALL OF SOUTH | CHENNAI</option>
					<option value="MODERN MALL | HYDERABAD">MODERN MALL | HYDERABAD</option>
				</select>
			
				<br>
				<input  class="form-control" type="text" id="name" name="name" placeholder="NAME" size="60px" required>
				<br>
				<input class="form-control" type="email" id="email" name="email" placeholder="EMAIL" size="60px" required>
				<br>
				<input class="form-control" type="number" id="phone" name="phone" placeholder="CONTACT NUMBER" maxlength="10" size="60px" required>
				<br>
				<textarea class="form-control" name="add" id="add" cols="30" rows="10" placeholder="ANY MESSAGE(OPTIONAL)" required></textarea>
				<br>
				<!-- <input class="form-control warning" type="submit"> -->
				<center><button type="submit"  class="btn btnall" >Submit </button></center>
	
		</form>
		
	</div>
<!-- main container ended -->
<!-- FOOTER -->
<?php 
	include('footer.php');
?>
<!-- FOOTER ENDED -->



<!-- Scripts -->


<!-- Bootstrap css plugin -->

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
</body>
</html>